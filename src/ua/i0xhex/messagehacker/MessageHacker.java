package ua.i0xhex.messagehacker;

import static ua.i0xhex.messagehacker.MessageManager.msg;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.plugin.java.JavaPlugin;

import com.comphenix.protocol.ProtocolLibrary;

import ua.i0xhex.messagehacker.transformer.MessageListener;
import ua.i0xhex.messagehacker.transformer.MessageTransformer;

public class MessageHacker extends JavaPlugin {
	public static MessageHacker plugin;
	
	@Override
	public void onEnable() {
		plugin = this;
		try {
			MessageManager.init(plugin);
			MessageTransformer.init();
			registerPacketListener();
		} catch (Exception ex) {
			Bukkit.getLogger().warning("[MessageHacker] Error occured when enabling plugin.");
			ex.printStackTrace();
		}
	}
	
	@Override
	public void onDisable() {
		unregisterPacketListener();
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {
		if (!sender.hasPermission("messagehacker.command.msghacker")) {
			sender.sendMessage(msg("command.no-permission"));
			return true;
		}
		if (args.length > 0) {
			switch (args[0].toLowerCase()) {
			case "reload":
				try {
					MessageTransformer.init();
				} catch (Exception ex) {
					Bukkit.getLogger().warning("[MessageHacker] Error occured when enabling plugin.");
					sender.sendMessage("§c[MessageHacker] Error. See console.");
					ex.printStackTrace();
				}
				sender.sendMessage(msg("command.reloaded"));
				return true;
			case "capture":
				if (args.length < 3) {
					sender.sendMessage(msg("command.capture-info"));
					return true;
				}
				int numOf = 1;
				try {
					numOf = Integer.parseInt(args[1]);
					if (numOf < 1) throw new NumberFormatException();
				} catch (NumberFormatException ex) {
					sender.sendMessage(msg("command.capture-invalid-number"));
					return true;
				}
				boolean json = false;
				if (args.length > 3 && args[3].equalsIgnoreCase("-json")) json = true;
				sender.sendMessage(msg("command.capture-started").replace("%numof%", numOf + ""));
				CaptureManager.query(numOf, args[2], sender.getName(), json);
				return true;
			}
		}
		sender.sendMessage(msg("command.info").replace("%version%", getDescription().getVersion()));
		return true;
	}
	
	private void registerPacketListener() {
		new MessageListener();
	}
	private void unregisterPacketListener() {
		ProtocolLibrary.getProtocolManager().removePacketListeners(plugin);
	}
}






















