package ua.i0xhex.messagehacker;

import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.Plugin;

public class MessageManager {
	public static HashMap<String, String> messages;
	
	public static void init(Plugin plugin) {
		messages = new HashMap<>();
		FileConfiguration msgCfg = loadCFG("messages.yml", plugin);
		for (String key : msgCfg.getConfigurationSection("").getKeys(true))
				messages.put(key, msgCfg.getString(key).replace('&', '§'));
	}
	
	public static String msg(String id) {
		if (!messages.containsKey(id)) {
			Bukkit.getLogger().warning("[MessageHacker] Message '" + id + "' not found! Check your messages.yml");
			return " ";
		}
		return messages.get(id);
	}
	private static FileConfiguration loadCFG(String FileName, Plugin plugin) {
		if (!new File(plugin.getDataFolder(), FileName).isFile()) {
			File file = new File(plugin.getDataFolder(), FileName);
			InputStreamReader default_ = null;
			try { default_ = new InputStreamReader(plugin.getResource(FileName), "UTF-8"); } catch (UnsupportedEncodingException e) {}
			try {YamlConfiguration.loadConfiguration(default_).save(file); } catch (IOException e) {}
			return YamlConfiguration.loadConfiguration(file);
		} else {
			File file = new File(plugin.getDataFolder(), FileName);
			return YamlConfiguration.loadConfiguration(file);
		}
	}
}
