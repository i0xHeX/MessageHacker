package ua.i0xhex.messagehacker.chat.component;

import java.util.ArrayList;

import com.jsoniter.JsonIterator;
import com.jsoniter.annotation.JsonIgnore;
import com.jsoniter.annotation.JsonProperty;
import com.jsoniter.output.JsonStream;

import ua.i0xhex.messagehacker.chat.ChatUtil;

public class TextComponent {
	@JsonProperty(defaultValueToOmit = "null")
	protected Boolean italic;
	@JsonProperty(defaultValueToOmit = "null")
	protected Boolean underlined;
	@JsonProperty(defaultValueToOmit = "null")
	protected Boolean strikethrough;
	@JsonProperty(defaultValueToOmit = "null")
	protected Boolean obfuscated;
	@JsonProperty(defaultValueToOmit = "null")
	protected Boolean bold;
	
	@JsonProperty(defaultValueToOmit = "null")
	protected TextColor color;
	@JsonProperty(defaultValueToOmit = "null")
	protected String insertion;
	@JsonProperty(defaultValueToOmit = "null")
	protected ActionComponent clickEvent;
	@JsonProperty(defaultValueToOmit = "null")
	protected ActionComponent hoverEvent;
	@JsonProperty(defaultValueToOmit = "null")
	protected ArrayList<TextComponent> extra;
	protected String text;
	
	public TextComponent() {
		this.text = "";
	}
	public static TextComponent fromText(String text) {
		text = ChatUtil.optimizeColors(text);
		ArrayList<TextComponent> extra = new ArrayList<>();
		TextComponent currentComponent = new TextComponent();
		StringBuilder textBuilder = new StringBuilder();
		boolean nextIsColor = false;
		
		for (char c : text.toCharArray()) {
			if (nextIsColor) {
				nextIsColor = false;
				TextColor color = TextColor.getByCode(c);
				if (color == TextColor.reset) color = TextColor.white;
				
				// Saving, pushing current component and creating new one
				if (textBuilder.length() != 0) {
					currentComponent.setText(textBuilder.toString());
					textBuilder.setLength(0);
					extra.add(currentComponent);
					TextComponent newComponent = new TextComponent();
					newComponent.italic = currentComponent.italic;
					newComponent.underlined = currentComponent.underlined;
					newComponent.strikethrough = currentComponent.strikethrough;
					newComponent.obfuscated = currentComponent.obfuscated;
					newComponent.bold = currentComponent.bold;
					newComponent.color = currentComponent.color;
					currentComponent = newComponent;
				}
				switch (color) {
				case italic:
					currentComponent.italic = true;
					break;
				case underline:
					currentComponent.underlined = true;
					break;
				case strikethrough:
					currentComponent.strikethrough = true;
					break;
				case obfuscated:
					currentComponent.obfuscated = true;
					break;
				case bold:
					currentComponent.bold = true;
					break;
				default:
					currentComponent.color = color;
					break;
				}
			} 
			else if (c == '§') nextIsColor = true;
			else textBuilder.append(c);
		}
		currentComponent.setText(textBuilder.toString());
		extra.add(currentComponent);
		
		if (extra.size() == 0) return extra.get(0);
		else {
			TextComponent component = new TextComponent();
			component.setExtra(extra);
			return component;
		}
	}
	public static TextComponent fromJson(String json) {
		return JsonIterator.deserialize(json, TextComponent.class);
	}
	
	// Operations
	public TextComponent withParent(TextComponent pc) {
		TextComponent mc = this.copy();
		
		// Merge formatting
		if (b(pc.italic) == 1 && b(mc.italic) == 0) mc.italic = null;
		else if ((b(pc.italic) == 1 && b(mc.italic) == -1)) mc.italic = true;
		
		if (b(pc.underlined) == 1 && b(mc.underlined) == 0) mc.underlined = null;
		else if ((b(pc.underlined) == 1 && b(mc.underlined) == -1)) mc.underlined = true;
		
		if (b(pc.strikethrough) == 1 && b(mc.strikethrough) == 0) mc.strikethrough = null;
		else if ((b(pc.strikethrough) == 1 && b(mc.strikethrough) == -1)) mc.strikethrough = true;
		
		if (b(pc.obfuscated) == 1 && b(mc.obfuscated) == 0) mc.obfuscated = null;
		else if ((b(pc.obfuscated) == 1 && b(mc.obfuscated) == -1)) mc.obfuscated = true;
		
		if (b(pc.bold) == 1 && b(mc.bold) == 0) mc.bold = null;
		else if ((b(pc.bold) == 1 && b(mc.bold) == -1)) mc.bold = true;
		
		if (mc.color == null) mc.color = pc.color;
		
		return mc;
	}
	public TextComponent copy() {
		TextComponent clonedComponent = new TextComponent();
		clonedComponent.italic = this.italic;
		clonedComponent.underlined = this.underlined;
		clonedComponent.strikethrough = this.strikethrough;
		clonedComponent.obfuscated = this.obfuscated;
		clonedComponent.bold = this.bold;
		
		clonedComponent.color = this.color;
		clonedComponent.insertion = this.insertion;
		if (this.clickEvent != null) {
			clonedComponent.clickEvent = new ActionComponent();
			clonedComponent.clickEvent.setAction(this.clickEvent.getAction());
			clonedComponent.clickEvent.setValue(this.clickEvent.getValue());
		}
		if (this.hoverEvent != null) {
			clonedComponent.hoverEvent = new ActionComponent();
			clonedComponent.hoverEvent.setAction(this.hoverEvent.getAction());
			clonedComponent.hoverEvent.setValue(this.hoverEvent.getValue());
		}
		if (this.extra != null) {
			clonedComponent.extra = new ArrayList<>();
			for (TextComponent extraComponent : this.extra) 
				clonedComponent.extra.add(extraComponent.copy());
		}
		clonedComponent.text = this.text;
		return clonedComponent;
	}
	public String toJson() {
		return JsonStream.serialize(this);
	}
	public String toText() {
		StringBuilder textBuilder = new StringBuilder();
		textBuilder.append('§').append(this.color != null ? this.color.getCode() : 'f');
		if (this.italic != null && this.italic) textBuilder.append("§o");
		if (this.underlined != null && this.underlined) textBuilder.append("§n");
		if (this.strikethrough != null && this.strikethrough) textBuilder.append("§m");
		if (this.obfuscated != null && this.obfuscated) textBuilder.append("§k");
		if (this.bold != null && this.bold) textBuilder.append("§l");
		if (!this.text.isEmpty()) textBuilder.append(this.text);
		else textBuilder.setLength(0);
		
		if (this.extra != null)
			for (TextComponent component : this.extra) {
				textBuilder.append(component.withParent(this).toText());
			}
		return ChatUtil.optimizeColors(textBuilder.toString());
	}
	public String toRawText() {
		StringBuilder textBuilder = new StringBuilder(this.text);
		if (this.extra != null) 
			for (TextComponent component : this.extra) 
				textBuilder.append(component.getText());
		return textBuilder.toString();
	}
	
	// Getters & Setters
	@JsonIgnore
	public Boolean getItalic() {
		return italic;
	}
	@JsonIgnore
	public Boolean getUnderlined() {
		return underlined;
	}
	@JsonIgnore
	public Boolean getStrikethrough() {
		return strikethrough;
	}
	@JsonIgnore
	public Boolean getObfuscated() {
		return obfuscated;
	}
	@JsonIgnore
	public Boolean getBold() {
		return bold;
	}
	@JsonIgnore
	public TextColor getColor() {
		return color;
	}
	@JsonIgnore
	public String getInsertion() {
		return insertion;
	}
	@JsonIgnore
	public ActionComponent getClickEvent() {
		return clickEvent;
	}
	@JsonIgnore
	public ActionComponent getHoverEvent() {
		return hoverEvent;
	}
	@JsonIgnore
	public ArrayList<TextComponent> getExtra() {
		return extra;
	}
	@JsonIgnore
	public String getText() {
		return text;
	}

	public void setItalic(Boolean italic) {
		this.italic = italic;
	}
	public void setUnderlined(Boolean underlined) {
		this.underlined = underlined;
	}
	public void setStrikethrough(Boolean strikethrough) {
		this.strikethrough = strikethrough;
	}
	public void setObfuscated(Boolean obfuscated) {
		this.obfuscated = obfuscated;
	}
	public void setBold(Boolean bold) {
		this.bold = bold;
	}
	public void setColor(TextColor color) {
		this.color = color;
	}
	public void setInsertion(String insertion) {
		this.insertion = insertion;
	}
	public void setClickEvent(ActionComponent clickEvent) {
		this.clickEvent = clickEvent;
	}
	public void setHoverEvent(ActionComponent hoverEvent) {
		this.hoverEvent = hoverEvent;
	}
	public void setExtra(ArrayList<TextComponent> extra) {
		this.extra = extra;
	}
	public void setText(String text) {
		this.text = text;
	}

	int b(Boolean bool) {
		if (bool == null) return -1;
		else return bool ? 1 : 0;
	}
}































