package ua.i0xhex.messagehacker.chat.component;

import java.util.HashMap;

public enum TextColor {
    black('0', false), 
    dark_blue('1', false), 
    dark_green('2', false), 
    dark_aqua('3', false), 
    dark_red('4', false), 
    dark_purple('5', false), 
    gold('6', false), 
    gray('7', false), 
    dark_gray('8', false), 
    blue('9', false), 
    green('a', false), 
    aqua('b', false), 
    red('c', false), 
    light_purple('d', false), 
    yellow('e', false), 
    white('f', false), 
    obfuscated('k', true), 
    bold('l', true), 
    strikethrough('m', true), 
    underline('n', true), 
    italic('o', true), 
    reset('r', false);
	
	private static final HashMap<Character, TextColor> colorMap;
	private final boolean isFormat;
	private final char code;
	
	static {
		colorMap = new HashMap<>();
		for (TextColor color : values()) colorMap.put(color.getCode(), color);
	}
	
	private TextColor(char code, boolean isFormat) {
		this.isFormat = isFormat;
		this.code = code;
	}
	
	public boolean isFormat() {return this.isFormat;}
	public char getCode() {return this.code;}
	
	public static TextColor getByCode(char code) {
		TextColor color = colorMap.get(code);
		if (color == null) return TextColor.white;
		else return color;
	}
}



























