package ua.i0xhex.messagehacker.chat;

public class ChatUtil {
	public static String optimizeColors(String in) {
		StringBuilder out = new StringBuilder();
		StringBuilder oldFormat = new StringBuilder("§r");
		StringBuilder newFormat = new StringBuilder("§r");
		StringBuilder formatChange = new StringBuilder();
		
		boolean nextIsColor = false;
		for (char c : in.toCharArray()) {
			// If next char means format / color code
			if (nextIsColor) {
				nextIsColor = false;
				// If char is format code
				if (c >= 'k' && c <= 'o') {
					boolean originalFormat = true;
					for (int i = 1; i < newFormat.length(); i += 2) {
						if (newFormat.charAt(i) != c) continue;
						originalFormat = false;
						break;
					}
					if (originalFormat) {
						newFormat.append('§').append(c);
						formatChange.append('§').append(c);
					}
				}
				// If char is color code
				else {
					if ((c < '0' || c > '9') && (c < 'a' || c > 'f')) c = 'f';
					newFormat.setLength(0);
					newFormat.append('§').append(c);
					formatChange.setLength(0);
					formatChange.append('§').append(c);
				}
			}
			else if (c == '§') nextIsColor = true;
			else {
				if (!newFormat.toString().equals(oldFormat.toString())) {
					out.append(formatChange);
					formatChange.setLength(0);
					oldFormat.setLength(0);
					oldFormat.append(newFormat);
				}
				out.append(c);
				if (c == '\n') {
					formatChange.append(oldFormat);
					newFormat.setLength(0);
					newFormat.append(oldFormat);
					oldFormat.setLength(0);
				}
			}
		}
		return out.toString();
	}
}





















