![MessageHacker Logo](https://i.imgur.com/5ksaF6k.png)

# MessageHacker

## About
MessageHacker - a tool, which will give you ability to handle every message in chat on your server (even from other plugins). Hate that some plugins do not provide you lang files to change messages? Want to turn off annoying spam messages? No problem! With this plugin you can modify anything.

## Features
- Change any server chat message on fly. (Translate untranslable plugins!)
- Change parts of every messages via regex.
- Replace words via %word<N>% placeholder
- Hide any server chat message.
- Regular expressions with capturing groups supported.
- PlaceholderAPI support
- JSON input / output supported (make your own hover or clickable text)
- Ability to ignore colors on input
- Multiple rule files, folders. Sort your rules as you want!

## Commands & Permissions
**`/msghacker reload`** - Reload rule configs<br>
**`/msghacker capture <N> <fileName> [-json]`** - capture next N messages and save to txt file.<br>
(Will appear in plugins/MessageHacker/capture folder)

**messagehacker.command.msghacker** - to use commands

## Dependencies
- ProtocolLib (Required)
- PlaceholderAPI (Optional)

[For more details - spigot page](https://www.spigotmc.org/resources/messagehacker.50492/)
